﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacao
{
    public interface IServicoContato
    {
        List<Contato> ObterTodosContatos();
        Contato SelecionarContato(int idContato);
        void SalvarContato(Contato contato);
        void DeletarContato(int idContato);
    }
}
