﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Map
{
    class TelefoneMap : EntityTypeConfiguration<Telefone>
    {
        public TelefoneMap()
        {
            ToTable("Telefone");

            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.Numero);

            Property(t => t.IdClassificacao);

            HasRequired(t => t.Classificacao)
            .WithMany(t => t.Telefones)
            .HasForeignKey(t => t.IdClassificacao);

            Property(t => t.IdContato);

            HasRequired(t => t.Contato)
            .WithMany(t => t.Telefones)
            .HasForeignKey(t => t.IdContato);

        }

    }
}
