namespace Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AtualizaObrigatoriedadeCampos2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Email", "IdContato", "dbo.Contato");
            DropIndex("dbo.Email", new[] { "IdContato" });
            AlterColumn("dbo.Email", "IdContato", c => c.Int(nullable: false));
            CreateIndex("dbo.Email", "IdContato");
            AddForeignKey("dbo.Email", "IdContato", "dbo.Contato", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Email", "IdContato", "dbo.Contato");
            DropIndex("dbo.Email", new[] { "IdContato" });
            AlterColumn("dbo.Email", "IdContato", c => c.Int());
            CreateIndex("dbo.Email", "IdContato");
            AddForeignKey("dbo.Email", "IdContato", "dbo.Contato", "Id");
        }
    }
}
