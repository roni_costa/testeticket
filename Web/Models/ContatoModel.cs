﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ContatoModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Empresa { get; set; }
        public string Endereco { get; set; }
        public List<TelefoneModel> Telefones { get; set; }
        public List<EmailModel> Emails { get; set; }

        public TelefoneModel Telefone1 { get; set; }
        public TelefoneModel Telefone2 { get; set; }
        public TelefoneModel Telefone3 { get; set; }

        public EmailModel Email1 { get; set; }
        public EmailModel Email2 { get; set; }
        public EmailModel Email3 { get; set; }

        public bool IsValid()
        {
            if (Nome == "" || Nome == null)
            {
                return false;
            }
            return true;
        }
    }
}