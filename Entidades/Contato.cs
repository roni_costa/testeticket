﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Contato
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Empresa { get; set; }
        public string Endereco { get; set; }
        public virtual ICollection<Telefone> Telefones { get; set; }
        public virtual ICollection<Email> Emails { get; set; }
    }
}
