﻿using Aplicacao;
using Aplicacao.Servicos;
using AutoMapper;
using Entidades.Interfaces;
using Persistencia.Context;
using Persistencia.Repositorios;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.App_Start;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Mapper.Initialize(x =>
            {
                x.AddProfile<ModelEntidadeProfile>();
            });


            //Simple Injector
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            #region Objetos injetados
            container.Register<IServicoContato, ServicoContato>();
            container.Register<IRepositorioContato, RepositorioContato>();
            container.Register<TicketAgendaDBContext>(new WebRequestLifestyle());
            #endregion

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

        }

    }
}
