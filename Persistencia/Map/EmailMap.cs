﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Map
{
    class EmailMap : EntityTypeConfiguration<Email>
    {
        public EmailMap()
        {
            ToTable("Email");

            HasKey(e => e.Id);

            Property(e => e.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(e => e.Endereco)
                .HasColumnType("varchar")
                .HasMaxLength(200);

            Property(t => t.IdClassificacao);

            HasRequired(t => t.Classificacao)
            .WithMany(t => t.Emails)
            .HasForeignKey(t => t.IdClassificacao);

            Property(t => t.IdContato);

            HasRequired(t => t.Contato)
            .WithMany(t => t.Emails)
            .HasForeignKey(t => t.IdContato);
        }
    }
}
