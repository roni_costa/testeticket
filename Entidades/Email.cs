﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Email
    {
        public int Id { get; set; }
        public string Endereco { get; set; }
        public int IdClassificacao { get; set; }
        public virtual Classificacao Classificacao { get; set; }
        public int IdContato { get; set; }
        public Contato Contato { get; set; }
    }
}
