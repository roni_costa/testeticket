namespace Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CriarTabelas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Email",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Endereco = c.String(maxLength: 200, unicode: false),
                        IdClassificacao = c.Int(nullable: false),
                        IdContato = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classificacao", t => t.IdClassificacao, cascadeDelete: true)
                .ForeignKey("dbo.Contato", t => t.IdContato, cascadeDelete: true)
                .Index(t => t.IdClassificacao)
                .Index(t => t.IdContato);
            
            CreateTable(
                "dbo.Contato",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        Empresa = c.String(maxLength: 100, unicode: false),
                        Endereco = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Telefone",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        IdClassificacao = c.Int(nullable: false),
                        IdContato = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Classificacao", t => t.IdClassificacao, cascadeDelete: true)
                .ForeignKey("dbo.Contato", t => t.IdContato, cascadeDelete: true)
                .Index(t => t.IdClassificacao)
                .Index(t => t.IdContato);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Email", "IdContato", "dbo.Contato");
            DropForeignKey("dbo.Telefone", "IdContato", "dbo.Contato");
            DropForeignKey("dbo.Telefone", "IdClassificacao", "dbo.Classificacao");
            DropForeignKey("dbo.Email", "IdClassificacao", "dbo.Classificacao");
            DropIndex("dbo.Telefone", new[] { "IdContato" });
            DropIndex("dbo.Telefone", new[] { "IdClassificacao" });
            DropIndex("dbo.Email", new[] { "IdContato" });
            DropIndex("dbo.Email", new[] { "IdClassificacao" });
            DropTable("dbo.Telefone");
            DropTable("dbo.Contato");
            DropTable("dbo.Email");
        }
    }
}
