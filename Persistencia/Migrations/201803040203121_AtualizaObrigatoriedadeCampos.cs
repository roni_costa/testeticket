namespace Persistencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AtualizaObrigatoriedadeCampos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Email", "IdContato", "dbo.Contato");
            DropIndex("dbo.Email", new[] { "IdContato" });
            AlterColumn("dbo.Email", "IdContato", c => c.Int());
            AlterColumn("dbo.Contato", "Nome", c => c.String(nullable: false, maxLength: 100, unicode: false));
            CreateIndex("dbo.Email", "IdContato");
            AddForeignKey("dbo.Email", "IdContato", "dbo.Contato", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Email", "IdContato", "dbo.Contato");
            DropIndex("dbo.Email", new[] { "IdContato" });
            AlterColumn("dbo.Contato", "Nome", c => c.String(maxLength: 100, unicode: false));
            AlterColumn("dbo.Email", "IdContato", c => c.Int(nullable: false));
            CreateIndex("dbo.Email", "IdContato");
            AddForeignKey("dbo.Email", "IdContato", "dbo.Contato", "Id", cascadeDelete: true);
        }
    }
}
