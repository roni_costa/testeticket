﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class TelefoneModel
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public int IdClassificacao { get; set; }
        public ClassificacaoModel Classificacao { get; set; }
    }
}