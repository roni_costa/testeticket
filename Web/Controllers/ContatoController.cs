﻿using Aplicacao;
using Aplicacao.Servicos;
using AutoMapper;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class ContatoController : Controller
    {
        private IServicoContato _servicoContato;

        public ContatoController(IServicoContato servicoContato)
        {
            _servicoContato = servicoContato;
        }

        public ActionResult Index()
        {
            var contatos = _servicoContato.ObterTodosContatos().OrderBy(c => c.Nome);
            return View(Mapper.Map<List<ListaContatoModel>>(contatos));
        }

        public ActionResult Incluir()
        {
            //Refatorar
            List<ClassificacaoModel> classificacoes = new List<ClassificacaoModel>();
            classificacoes.Add( new ClassificacaoModel { Id = 1,  Descricao = "Casa" });
            classificacoes.Add(new ClassificacaoModel { Id = 2, Descricao = "Trabalho" });
            classificacoes.Add(new ClassificacaoModel { Id = 3, Descricao = "Outro" });
            ViewBag.Classificacoes = classificacoes;
            return View();
        }

        [HttpPost]
        public ActionResult Gravar(ContatoModel contato)
        {
            if (contato.IsValid())
            {
                //temporário Apenas para converter no obj os n telefones e emails de forma mais fácil
                //refatorar 
                List<TelefoneModel> telefones = new List<TelefoneModel>();
                if (contato.Telefone1.Numero != null)
                    telefones.Add(contato.Telefone1);
                if (contato.Telefone2.Numero != null)
                    telefones.Add(contato.Telefone2);
                if (contato.Telefone3.Numero != null)
                    telefones.Add(contato.Telefone3);
                contato.Telefones = telefones;
                //refatorar 
                List<EmailModel> emails = new List<EmailModel>();
                if (contato.Email1.Endereco != null)
                    emails.Add(contato.Email1);
                if (contato.Email2.Endereco != null)
                    emails.Add(contato.Email2);
                if (contato.Email3.Endereco != null)
                    emails.Add(contato.Email3);
                contato.Emails = emails;

                _servicoContato.SalvarContato(Mapper.Map<Contato>(contato));
                return RedirectToAction("Index");
            }
            else
            {
                return View(contato);
            }
        }

        public ActionResult Editar(int id)
        {
            var contato = _servicoContato.SelecionarContato(id);
            return View(Mapper.Map<ContatoModel>(contato));
        }

        public ActionResult Visualizar(int id)
        {
            var contato = _servicoContato.SelecionarContato(id);
            return View(Mapper.Map<ContatoModel>(contato));
        }

        [HttpPost]
        public ActionResult Deletar(int id)
        {
            _servicoContato.DeletarContato(id);

            var contatos = _servicoContato.ObterTodosContatos().OrderBy(c => c.Nome);
 
            return Json(Mapper.Map<List<ListaContatoModel>>(contatos));
        }
        
    }
}