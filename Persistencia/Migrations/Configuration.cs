namespace Persistencia.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Entidades;

    internal sealed class Configuration : DbMigrationsConfiguration<Persistencia.Context.TicketAgendaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Persistencia.Context.TicketAgendaDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.Classificacoes.Add(new Classificacao { Descricao = "Casa" });
            context.Classificacoes.Add(new Classificacao { Descricao = "Trabalho" });
            context.Classificacoes.Add(new Classificacao { Descricao = "Outro" });
        }
    }
}
