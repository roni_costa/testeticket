﻿using Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistencia.Map
{
    class ClassificacaoMap : EntityTypeConfiguration<Classificacao>
    {
        public ClassificacaoMap()
        {
            ToTable("Classificacao");

            HasKey(c => c.Id);

            Property(c => c.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(c => c.Descricao)
                .HasColumnType("varchar")
                .HasMaxLength(20);
        }
    }
}
