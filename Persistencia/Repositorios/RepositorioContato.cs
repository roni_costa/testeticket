﻿using Entidades.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Persistencia.Context;

namespace Persistencia.Repositorios
{
    public class RepositorioContato : IRepositorioContato
    {
        private TicketAgendaDBContext _db;
        public RepositorioContato(TicketAgendaDBContext ticketAgendaDBContext)
        {
            _db = ticketAgendaDBContext;
        }
        public void Deletar(int idContato)
        {
            Contato contato = Selecionar(idContato);

            if (contato != null)
            {
                _db.Contatos.Remove(contato);
                _db.SaveChanges();
            }

        }

        public void Gravar(Contato contato)
        {
            Contato contatoDb = _db.Contatos.FirstOrDefault(a => a.Id == contato.Id);

            if (contatoDb != null)
            {
                _db.Entry(contatoDb).CurrentValues.SetValues(contato);

                // Deleta telefones
                foreach (var telefoneDb in contatoDb.Telefones.ToList())
                {
                    if (!contato.Telefones.Any(c => c.Id == telefoneDb.Id))
                        _db.Telefones.Remove(telefoneDb);
                }

                // Insere ou Atualiza Telefones
                foreach (var telefone in contato.Telefones)
                {
                    var telefoneDb = contatoDb.Telefones.FirstOrDefault(c => c.Id == telefone.Id);

                    if (telefoneDb != null)
                        _db.Entry(telefoneDb).CurrentValues.SetValues(telefone);
                    else
                    {
                        var novoTelefone = new Telefone
                        {
                            Numero = telefone.Numero,
                            Classificacao = telefone.Classificacao
                        };
                        contatoDb.Telefones.Add(novoTelefone);
                    }
                }

                // Deleta Emails
                foreach (var emailDb in contatoDb.Emails.ToList())
                {
                    if (!contato.Emails.Any(c => c.Id == emailDb.Id))
                        _db.Emails.Remove(emailDb);
                }

                // Insere ou Atualiza Emails
                foreach (var email in contato.Emails)
                {
                    var emailDb = contatoDb.Emails.FirstOrDefault(c => c.Id == email.Id);

                    if (emailDb != null)
                        _db.Entry(emailDb).CurrentValues.SetValues(email);
                    else
                    {
                        var novoEmail = new Email
                        {
                            Endereco = email.Endereco,
                            Classificacao = email.Classificacao
                        };
                        contatoDb.Emails.Add(novoEmail);
                    }
                }
            }
            else
            {
                _db.Contatos.Add(contato);
            }
            _db.SaveChanges();
        }

        public List<Contato> ListarTodos()
        {
            return _db.Contatos.ToList();
        }

        public Contato Selecionar(int idContato)
        {
            return _db.Contatos.FirstOrDefault(a => a.Id == idContato);
        }
    }
}
