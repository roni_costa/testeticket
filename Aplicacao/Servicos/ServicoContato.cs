﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using Entidades.Interfaces;
using Persistencia.Repositorios;

namespace Aplicacao.Servicos
{
    public class ServicoContato : IServicoContato
    {
        private IRepositorioContato _repositorioContato;
        public ServicoContato(IRepositorioContato repositorioContato)
        {
            _repositorioContato = repositorioContato;
        }
                
        public void DeletarContato(int idContato)
        {
            _repositorioContato.Deletar(idContato);
        }

        public List<Contato> ObterTodosContatos()
        {
            return _repositorioContato.ListarTodos();
        }

        public void SalvarContato(Contato contato)
        {
            _repositorioContato.Gravar(contato);
        }

        public Contato SelecionarContato(int idContato)
        {
            return _repositorioContato.Selecionar(idContato);
        }
    }
}
