﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Models
{
    public class ClassificacaoModel
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}