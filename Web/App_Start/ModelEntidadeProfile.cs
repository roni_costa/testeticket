﻿using AutoMapper;
using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.App_Start
{
    public class ModelEntidadeProfile : Profile
    {
        public ModelEntidadeProfile() : base("ModelEntidadeProfile")
        {
            CreateMap<Contato, ListaContatoModel>();
            CreateMap<ContatoModel, Contato>();
            CreateMap<Contato, ContatoModel>();
            CreateMap<TelefoneModel, Telefone>();
            CreateMap<EmailModel, Email>();
            CreateMap<ClassificacaoModel, Classificacao>();
            CreateMap<Classificacao, ClassificacaoModel>();
        }
    }
}