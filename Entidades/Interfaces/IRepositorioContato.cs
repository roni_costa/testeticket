﻿using Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Interfaces
{
    public interface IRepositorioContato
    {
        List<Contato> ListarTodos();
        Contato Selecionar(int idContato);
        void Gravar(Contato contato);
        void Deletar(int idContato);
    }
}
